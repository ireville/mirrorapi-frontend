import React, { useCallback, useState, useEffect, useMemo } from 'react';
import cn from 'classnames';
import { useHistory } from 'react-router-dom';
import {
    NETWORK_TYPES,
    ROLES_ARR,
    DEFAULT_PARTY,
    DEFAULT_ATTRIBUTE,
    NETWORK_TYPES_LIST,
    Deal,
    ClientFull,
} from '../../types';
import { fetchClients, fetchCreateDealFree } from '../../api';
import { getCurrentScenario, setNetworkType, setDealUid } from '../../utils';
import { ROUTES } from '../../routes';

import './style.scss';

export const ConfigPage: React.FC = () => {
    const history = useHistory();
    const scenario = useMemo(getCurrentScenario, []);
    const [deal, setDeal] = useState<Partial<Deal>>({ ...scenario.deal });
    const [type, setType] = useState<NETWORK_TYPES>(NETWORK_TYPES.Quorum);
    const [clients, setClients] = useState<ClientFull[]>([]);
    const [showLoading] = useState<boolean>(false);

    const handleAppendParticipant = useCallback(
        e => {
            e.preventDefault();
            setDeal(_deal => {
                return {
                    ...deal,
                    parties: [...(_deal.parties || []), { ...DEFAULT_PARTY }],
                };
            });
        },
        [deal],
    );

    const handleSelectType = useCallback(
        (_typeID: NETWORK_TYPES) => () => {
            setType(_typeID);
        },
        [setType],
    );

    useEffect(() => {
        setNetworkType(type);
    }, [type]);

    const handleSelectParticipantClient = useCallback(
        index => (e: React.ChangeEvent<HTMLSelectElement>) => {
            const key = e.target.value;
            setDeal(_deal => {
                const newParties = _deal.parties || [];
                newParties[index] = { ...newParties[index], key };
                return {
                    ..._deal,
                    parties: newParties,
                };
            });
        },
        [],
    );

    const handleSelectParticipantRole = useCallback(
        index => (e: React.ChangeEvent<HTMLSelectElement>) => {
            const role = e.target.value;
            setDeal(_deal => {
                const newParties = _deal.parties || [];
                newParties[index] = { ...newParties[index], role };
                return {
                    ..._deal,
                    parties: newParties,
                };
            });
        },
        [],
    );

    const handleDeleteParticipant = useCallback(
        index => () => {
            setDeal(_deal => {
                const newParties = _deal.parties || [];
                return {
                    ..._deal,
                    parties: newParties.filter((_el, _index) => _index !== index),
                };
            });
        },
        [],
    );

    useEffect(() => {
        (async () => {
            const newClients = await fetchClients(type);
            setClients(newClients);
            setDeal(_deal => {
                const newParties = _deal.parties || [];
                return {
                    ..._deal,
                    parties: newParties.map((_el, index) => ({
                        ..._el,
                        key: newClients[index]?.key || newClients[0]?.key,
                    })),
                };
            });
        })();
    }, [type]);

    const handleClickAppendParameter = useCallback(() => {
        setDeal(_deal => {
            return {
                ..._deal,
                attributes: [...(_deal.attributes?.filter(attribute => attribute.key !== null) || []), { ...DEFAULT_ATTRIBUTE }],
            };
        });
    }, []);

    const handleClickDeleteParameter = useCallback(
        index => () => {
            setDeal(_deal => {
                return {
                    ..._deal,
                    attributes: (_deal.attributes || []).filter((_el, _index) => _index !== index),
                };
            });
        },
        [],
    );

    const handleChangeAttributeKey = useCallback(
        index => (e: React.ChangeEvent<HTMLInputElement>) => {
            const key = e.target.value;
            setDeal(_deal => {
                const newParameters = _deal.attributes || [];
                newParameters[index] = { ...newParameters[index], key };
                return {
                    ..._deal,
                    attributes: newParameters,
                };
            });
        },
        [],
    );

    const handleGetKind = useCallback((e) => {
        const value = e?.target?.value;
        setDeal(_deal => {
            const newKind = value || "";
            return {
                ..._deal,
                kind: newKind,
            };
        });},
    []);

    const handleGetData = useCallback((e) => {
        const value = e?.target?.value;
        setDeal(_deal => {
            const newData = value || "";
            return {
                ..._deal,
                data: newData,
            };
        });},
    []);

    const handleGetDealsUuid = useCallback((e) => {
        const value = e?.target?.value;
        setDeal(_deal => {
            const newDealsUuid = value || "";
            return {
                ..._deal,
                dealsUuid: newDealsUuid,
            };
        });},
    []);

    const handleGetParent = useCallback((e) => {
        const value = e?.target?.value;
        setDeal(_deal => {
            const newParent = value || "";
            return {
                ..._deal,
                parent: newParent,
            };
        });},
    []);
    

    const handleChangeAttributeValue = useCallback(
        index => (e: React.ChangeEvent<HTMLInputElement>) => {
            const { value } = e.target;
            setDeal(_deal => {
                const newParameters = _deal.attributes || [];
                newParameters[index] = { ...newParameters[index], value };
                return {
                    ..._deal,
                    attributes: newParameters,
                };
            });
        },
        [],
    );

    const handleClickStart = useCallback(() => {
        (async () => {
            const result = await fetchCreateDealFree(deal);
            setDealUid(result);
            history.push(ROUTES.MAIN);
        })();
    }, [deal, history]);

    return (
        <div className="row d-flex h-100 justify-content-center align-items-center">
            {showLoading ? <div className="loading">Processing</div> : null}
            <div className="card col-lg-8 col-md-12 col-sm-12">
                <div className="card-body">
                    <div>
                        <h3 className="card-title   ">
                            <b>Step 2.</b>
                            Configuring the scenario
                        </h3>
                        <div className="mb-3 pb-3 ">
                            <h5 className="card-title">Technology</h5>
                            <div>
                                <ul className="list-group list-group-horizontal list-types">
                                    {NETWORK_TYPES_LIST.map(_type => (
                                        <li
                                            key={_type.id}
                                            role="presentation"
                                            className={cn([
                                                'list-group-item list-group-item-action text-center ',
                                                type === _type.id && 'active',
                                            ])}
                                            onClick={handleSelectType(_type.id)}>
                                            <strong>{_type.value}</strong>
                                        </li>
                                    ))}
                                </ul>
                            </div>
                        </div>

                        {/* Kind */}
                        <div className="mb-3 pb-3 ">
                            <h5 className="card-title">Kind</h5>
                            <div className="form-row">
                                <div className="form-group col-md-5">
                                    <input
                                        type="text"
                                        className="form-control"
                                        placeholder="Field name"
                                        onChange={handleGetKind}
                                        value={deal?.kind} />
                                </div>
                            </div>
                        </div>

                        {/* Data */}
                        <div className="mb-3 pb-3 ">
                            <h5 className="card-title">Data</h5>
                            <div className="form-row">
                                <div className="form-group col-md-5">
                                    <input
                                        type="text"
                                        className="form-control"
                                        placeholder="Field name"
                                        onChange={handleGetData}
                                        value={deal?.data} />
                                </div>
                            </div>
                        </div>

                        {/* DealsUuid */}
                        <div className="mb-3 pb-3 ">
                            <h5 className="card-title">DealsUuid</h5>
                            <div className="form-row">
                                <div className="form-group col-md-5">
                                    <input
                                        type="text"
                                        className="form-control"
                                        placeholder="Field name"
                                        onChange={handleGetDealsUuid}
                                        value={deal?.dealsUuid} />
                                </div>
                            </div>
                        </div>

                        {/* Parent */}
                        <div className="mb-3 pb-3 ">
                            <h5 className="card-title">Parent</h5>
                            <div className="form-row">
                                <div className="form-group col-md-5">
                                    <input
                                        type="text"
                                        className="form-control"
                                        placeholder="Field name"
                                        onChange={handleGetParent}
                                        value={deal?.parent?.toString()} />
                                </div>
                            </div>
                        </div>


                        {/* Participant */}
                        <div className="mb-3 pb-3 ">
                            <h5 className="card-title">Parties</h5>
                            <div>
                                {deal?.parties?.map((party, index) => (
                                    <div key={`${party?.key}${party?.role}`} className="form-row">
                                        <div className="col-sm-2 col-form-label">{`Party ${index + 1}`}</div>
                                        
                                        <div className="form-group col-md-5">
                                            <select
                                                className="form-control"
                                                onChange={handleSelectParticipantClient(index)}
                                                value={party.key}>
                                                {clients.map(_client => (
                                                    <option key={_client.key} value={_client.key}>
                                                        {_client.info}
                                                    </option>
                                                ))}

                                                {/*deal?.parties?.map(_party => (
                                                    <option key={party?.key} value={party?.key}>
                                                        {party?.role}
                                                    </option>
                                                ))*/}
                                            </select>
                                        </div>

                                        <div className="form-group col-md-4">
                                            <select
                                                disabled={!scenario.canChangePartyRole}
                                                className="form-control"
                                                onChange={handleSelectParticipantRole(index)}
                                                value={party.role}>
                                                <option> -- Choose Role</option>
                                                {ROLES_ARR.map(_role => (
                                                    <option key={_role} value={_role}>
                                                        {_role}
                                                    </option>
                                                ))}
                                            </select>
                                        </div>
                                        {scenario.canDeleteParty ? (
                                            <div className="form-group col-md-1">
                                                {index > 0 ? (
                                                    <button
                                                        type="button"
                                                        className="btn  btn-outline-danger"
                                                        onClick={handleDeleteParticipant(index)}>
                                                        Delete
                                                    </button>
                                                ) : null}
                                            </div>
                                        ) : null}
                                    </div>
                                ))}
                                {scenario.canAppendParty ? (
                                    <button
                                        type="button"
                                        className="btn btn-primary"
                                        onClick={handleAppendParticipant}>
                                        Append participant
                                    </button>
                                ) : null}
                            </div>
                        </div>
                        {/* Attributes */}
                        <div className="mb-3 ">
                            <h5 className="card-title">Attributes</h5>
                            <div>
                                {deal.attributes?.length === 0 ? (
                                    <div className="text-muted mb-3">
                                        Attributes are empty, set attributes
                                    </div>
                                ) : (
                                    deal.attributes?.map((attribute, index) => (
                                        <div key={attribute.key} className="form-row">
                                            <div className="col-sm-2 col-form-label">
                                                {`Attribute ${index + 1}`}
                                            </div>
                                            <div className="form-group col-md-5">
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    placeholder="Field name"
                                                    onChange={handleChangeAttributeKey(index)}
                                                    value={attribute.key}
                                                />
                                            </div>
                                            <div className="form-group col-md-4">
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    placeholder="Field value"
                                                    onChange={handleChangeAttributeValue(index)}
                                                    value={attribute.value}
                                                />
                                            </div>
                                            <div className="form-group col-md-1">
                                                <button
                                                    type="button"
                                                    className="btn  btn-outline-danger"
                                                    onClick={handleClickDeleteParameter(index)}>
                                                    Delete
                                                </button>
                                            </div>
                                        </div>
                                    ))
                                )}

                                <button
                                    type="button"
                                    className="btn btn-primary"
                                    onClick={handleClickAppendParameter}>
                                    Append Parameter
                                </button>
                            </div>
                        </div>
                        {/* STATUSES */}
                        <div className="mb-3 ">
                            <h5 className="card-title">Status map</h5>
                            <div>
                                {deal.statusMaps?.map((statusItem, index) => (
                                    <div
                                        className="form-row"
                                        key={
                                            statusItem.status + statusItem.statusNext + statusItem.roles?.[0]
                                        }>
                                        <div className="col-sm-2 col-form-label">{`Status ${index + 1}`}</div>
                                        <div className="form-group col-md-3">
                                            <input
                                                disabled={!scenario.canChangeStatusMap}
                                                type="text"
                                                className="form-control"
                                                placeholder="Status"
                                                value={statusItem.status}
                                            />
                                        </div>
                                        <div className="form-group col-md-3">
                                            <input
                                                disabled={!scenario.canChangeStatusMap}
                                                type="text"
                                                className="form-control"
                                                placeholder="Next Status"
                                                value={statusItem.statusNext}
                                            />
                                        </div>
                                        <div className="form-group col-md-3">
                                            <select
                                                className="form-control"
                                                disabled={!scenario.canChangeStatusMap}
                                                value={statusItem.roles?.[0]}>
                                                {ROLES_ARR.map(_role => (
                                                    <option
                                                        key={_role}
                                                        value={_role}
                                                        disabled={!scenario.canChangeStatusMap}>
                                                        {_role}
                                                    </option>
                                                ))}
                                            </select>
                                        </div>
                                        {scenario.canChangeStatusMap ? (
                                            <div className="form-group col-md-1">
                                                <button type="button" className="btn  btn-outline-danger">
                                                    Delete
                                                </button>
                                            </div>
                                        ) : null}
                                    </div>
                                ))}
                                {scenario.canChangeStatusMap ? (
                                    <button type="button" className="btn btn-outline-primary">
                                        Append status map
                                    </button>
                                ) : null}
                            </div>
                        </div>
                        <div className="mb-3 ">
                            <button
                                type="button"
                                className="btn btn-lg btn-primary"
                                onClick={handleClickStart}>
                                Start Scenario
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
