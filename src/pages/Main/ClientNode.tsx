import React, { useMemo, useState, useEffect } from 'react';
import { BackendDealResponse, ClientFull } from '../../types';
import { DealInfo } from './DealInfo';
import { RequestsList } from './RequestsList';

export interface ClientNodeProps {
    client: ClientFull;
    dealResp: BackendDealResponse;
}

export const ClientNode: React.FC<ClientNodeProps> = (props: ClientNodeProps) => {
    const { client } = props;
    const {dealResp} = props;
    
    const { deal, dealParties } = dealResp;

    const [applyStatuses, setApplyStatuses] = useState<string[]>([]);
    const clientUrl = client?.url || '';


    useEffect(() => {
        setApplyStatuses([]);
    }, [deal?.status]);

    const currentRole = useMemo(() => {
        if (dealParties) {
            return dealParties?.find(party => party.role === client.key)?.role;
        }
        return '';
    }, [client, deal]);

    return (
        <div>
            <div className="pt-3">
                <h3>
                    <b>NODE</b>
                    {` ${currentRole || ''}`}
                    <div className="small" style={{ fontSize: '14px' }}>
                        {clientUrl}
                    </div>
                </h3>
            </div>
            <div>
                {dealResp ? (
                    <div>
                        <div>
                            <DealInfo dealResp={dealResp} clientUrl={clientUrl} />
                        </div>
                        { /* REQUESTS */ }
                        <div>
                            <RequestsList  resp={dealResp} />
                        </div>
                    </div>
                ) : (
                    'Waiting information...'
                )}
            </div>
        </div>
    );
};
