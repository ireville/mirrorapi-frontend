import React, { useState, useEffect } from 'react';
import { fetchCurrentDeal, findClientByKey } from '../../utils';
import { ClientFull, BackendDealResponse } from '../../types';
import { ClientNode } from './ClientNode';
import { Graph } from '../../components/Graph';

export const MainPage: React.FC = () => {
    const [dealResponse, setBackendDealResponse] = useState<BackendDealResponse>();
    const [clients] = useState<ClientFull[]>([]);
    useEffect(() => {
        (async () => {
            const { data: newBackDealResp } = await fetchCurrentDeal();
            setBackendDealResponse(newBackDealResp);
        })();
    }, []);
    const url = clients[0]?.url;
    return (
        <div className="container-fluid">
            {dealResponse ? (
                <div className="row">
                    <div key={dealResponse.dealParties[0].role} className="col-lg col-xl border-right">
                            <ClientNode client={findClientByKey(clients || [], dealResponse.dealParties[0].role)} dealResp={dealResponse}  />
                    </div>
                    <div className="col-lg col-xl ">
                        <Graph url={url} />
                    </div>
                </div>
            ) : null}
        </div>
    );
};
