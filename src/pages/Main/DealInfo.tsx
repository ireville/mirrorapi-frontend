import React from 'react';
import { BackendDealResponse } from '../../types';
import './style.scss';

export interface IDealProps {
    dealResp: BackendDealResponse;
    clientUrl: string;
}

export const DealInfo: React.FC<IDealProps> = (props: IDealProps) => {
    const { dealResp } = props;
    const { deal, dealAttributes, dealFiles, dealParties } = dealResp;
    return (
        <div>
            <div>
                <h4>
                    <b>Deal</b>
                </h4>
            </div>
            <div className="row">
                <div className="col titleWidth">
                    <b>LocalID:</b>{' '}
                </div>
                <div className="col"> {deal.id} </div>
            </div>

            <div className="row">
                <div className="col titleWidth">
                    <b>Kind:</b>{' '}
                </div>
                <div className="col"> {deal.kind} </div>
            </div>

            <div className="row">
                <div className="col titleWidth">
                    <b>Status:</b>{' '}
                </div>
                <div className="col">{deal.status}</div>
            </div>

            <div className="row">
                <div className="col titleWidth">
                    <b>BlockChainId:</b>{' '}
                </div>
                <div className="col">{deal.blockChainId}</div>
            </div>

            <div className="row">
                <div className="col titleWidth">
                    <b>Data:</b>{' '}
                </div>
                <div className="col">{deal.data}</div>
            </div>

            <div className="row">
                <div className="col titleWidth">
                    <b>DealsUuid:</b>{' '}
                </div>
                <div className="col" style={{ wordBreak: 'break-all' }}>
                    {deal.dealsUuid}
                </div>
            </div>

            {dealAttributes ? (
                <div className="mt-4">
                    <div className="h4">
                        <b>Attributes</b>
                    </div>
                    {dealAttributes.length && dealAttributes[0].key != null ? (
                        dealAttributes.filter(param => param.key != null).map(param => {
                            return (
                                <div className="row" key={param.key}>
                                    <div className="col-3 titleWidth">
                                        <b>{`${param.key}: `}</b>
                                    </div>
                                    <div className="col">{param.value}</div>
                                </div>
                            );
                        }))  : ( <div> No attributes </div> )
                    }
                </div>
            ) : null}

            {dealParties ? (
                <div className="mt-4">
                    <div className="h4">
                        <b>Parties</b>
                    </div>
                    {dealParties.length ? (
                        dealParties.map((param, index) => {
                        return (
                            <div className="row" key={param.key}>
                                <div className="col-3 titleWidth">
                                    <b>{`Party ${index + 1} role: `}</b>
                                </div>
                                <div className="col">{param.role}</div>
                            </div>
                        );
                        }))  : ( <div> No parties </div> )
                    }
                </div>
            ) : null}

            {dealFiles ? (
                <div className="mt-4">
                    <div className="h4">
                        <b>Files</b>
                    </div>
                    {dealFiles.length ? (
                        dealFiles.map(file => (
                            <div>
                                File
                                <div className="row" key={file.id}>
                                    <div className="col-3 titleWidth">
                                        <b>{`${file.id}: `}</b>
                                    </div>
                                </div>
                            </div>
                        ))
                    ) : (
                        <div> No files</div>
                    )}
                </div>
            ) : null}
        </div>
    );
};
