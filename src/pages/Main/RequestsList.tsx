import React, { useCallback, useState } from 'react';
import cn from 'classnames';
import { BackendDealResponse } from '../../types';
import './style.scss';
import { RequestModal } from '../../components/RequestModal';

export interface IRequestsListProps {
    resp: BackendDealResponse;
}

export const RequestsList: React.FC<IRequestsListProps> = (props: IRequestsListProps) => {
    const {resp } = props;
    const [show, setShow] = useState<Boolean>(false);

    const handleShowRequestModal = useCallback(
        () => () => {
            setShow(true);
        },
        [],
    );

    const handleCloseRequestModal = useCallback(() => {
        setShow(false);
    }, []);

    return (
        <div>
            {show ? (
                <RequestModal resp={resp} onHide={handleCloseRequestModal} />
            ) : null}
            <div className="h4 mt-4">
                <b>{`Response`}</b>
            </div>
            {resp ? (
                <ul className="list-group custom-list border">
                    <li
                        role="presentation"
                        className={cn(['list-group-item request-item'])}
                        onClick={handleShowRequestModal()} >
                        {<div style={{ wordBreak: 'break-all' }}></div>}
                        <div className="d-flex justify-content-between">
                            <text>{`Click MORE to see the full response information`}</text>
                            <button type="button" className="btn btn-sm btn-link small">
                                more
                            </button>
                        </div>
                    </li>
                </ul>
            ) : (
                '-'
            )}
        </div>
    );
};
