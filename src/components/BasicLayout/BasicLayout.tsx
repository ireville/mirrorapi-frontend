import React, { useCallback } from 'react';
import './style.scss';
import { useHistory } from 'react-router-dom';
import { ROUTES } from '../../routes';

export interface BasicLayoutProps {
    children: React.ReactNode;
}
export const BasicLayout: React.FC<BasicLayoutProps> = (props: BasicLayoutProps) => {
    const { children } = props;
    const history = useHistory();
    const handleClickExit = useCallback(() => {
        localStorage.clear();
        history.push(ROUTES.SCENARIO);
    }, [history]);
    return (
        <div className="basic-layout h-100">
            <div className=" basic-layout-nav">
                <div className="container">
                    <div className="d-flex justify-content-between">
                        <div className="d-flex">
                            <img
                                src="/MirrorAPI_Logo.png"
                                alt="Kekker logo"
                                className="logo"
                                height="655"
                                width="155"
                            />
                            <h3 className="ml-3 text-white font-weight-bold">DEMO</h3>
                        </div>
                        <button
                            type="button"
                            className="btn btn-primary btn-sm align-self-center"
                            onClick={handleClickExit}
                            style={{ marginTop: '-7px' }}>
                            Exit and try again
                        </button>
                    </div>
                </div>
            </div>
            <div className="basic-layout-content">{children}</div>
        </div>
    );
};
