import React from 'react';
import Modal, { ModalProps } from 'react-bootstrap/Modal';
import { BackendDealResponse } from '../../types';

export interface RequestModalProps extends ModalProps {
    resp: BackendDealResponse;
}

export const RequestModal: React.FC<RequestModalProps> = (props: RequestModalProps) => {
    const { resp, onHide, ...otherProps } = props;
    return (
        <Modal {...otherProps} onHide={onHide} show size="lg">
            <Modal.Header closeButton onHide={onHide}>
                <Modal.Title>Request information</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div>
                    <div>
                        <div>
                            <b>Request</b>
                        </div>
                        <div>{`POST`}</div>
                    </div>

                    <div>
                        <div>
                            <b>Response</b>
                        </div>
                        <pre className="custom-item-content ml-5">
                            {JSON.stringify(resp, null, 2) || 'Empty'}
                        </pre>
                    </div>
                </div>
            </Modal.Body>
            <Modal.Footer>
                <button type="button" className="btn btn-primary" onClick={onHide}>
                    Close
                </button>
            </Modal.Footer>
        </Modal>
    );
};
