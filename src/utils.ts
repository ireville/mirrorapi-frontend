import { SCENARIOS } from './scenarios';
import { NETWORK_TYPES, ClientFull } from './types';
import { fetchDealByUid } from './api';

export const getCurrentScenario = () => {
    const scenarioId = localStorage.getItem('scenarioId');
    return SCENARIOS.find(scenario => scenario.id === scenarioId) || SCENARIOS[0];
};

export const setScenarioId = (scenarioId: string) => {
    localStorage.setItem('scenarioId', scenarioId);
};
export const getNetworkType = () => {
    return localStorage.getItem('type') as NETWORK_TYPES;
};

export const setNetworkType = (type: NETWORK_TYPES) => {
    localStorage.setItem('type', type);
};

export const getDealUid = () => {
    return localStorage.getItem('dealUid') as string;
};

export const setDealUid = (localDealId: number) => {
    localStorage.setItem('dealUid', String(localDealId));
    
};

export const getAuthHeaders = () => {
    return {
        Authorization: 'Basic S2Vra2VyVXNlcjp6RGZqbTMz',
        Channel: 'isychev199@gmail.com',
        'x-api-version': '2.0',
    };
};

export const fetchCurrentDeal = async (url?: string) => {
    const dealUid = getDealUid();
    return fetchDealByUid(dealUid, url);
};

export const findClientByKey = (clients: ClientFull[], key: string): ClientFull => {
    const clientss = [{info: "Mirror 1", key: "Seller"}, {info: "Mirror 2", key: "Buyer"}, {info: "Mirror 3", key: "Bank"}];
    return clientss.find(client => client.key === key) as ClientFull;
};
