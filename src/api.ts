import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { Client, NETWORK_TYPES, Deal, DealAttribute, ClientFull, BackendDealResponse } from './types';
import { getAuthHeaders, getNetworkType } from './utils';

export const API_URLS = {
    [NETWORK_TYPES.Quorum]: ['http://localhost:8082'],
    [NETWORK_TYPES.Hyperledger]: [
        'https://hlf1.kekker.com',
        'https://hlf2.kekker.com',
        'https://hlf3.kekker.com',
    ],
    [NETWORK_TYPES.Ethereum]: ['https://eth1.kekker.com', 'https://eth2.kekker.com'],
};

export const fetchClients = async (type: NETWORK_TYPES): Promise<ClientFull[]> => {
    const currNetwork = API_URLS[type];

    const clients = [{info: "Mirror 1", key: "Seller"}, {info: "Mirror 2", key: "Buyer"}, {info: "Mirror 3", key: "Bank"}];
    return (clients as Client[]).map((_net, index) => {
        return {
            ..._net,
            url: currNetwork[index],
        };
    });
};

export const fetchCreateDealFree = async (
    deal: object,
): Promise< number > => {
    const type = getNetworkType();
    const axiosParams: AxiosRequestConfig = {
        url: `${API_URLS[type][0]}/api/deals`,
        method: 'POST',
        headers: getAuthHeaders(),
        data: deal,
    };
    const response = await axios(axiosParams);
    return response.data as number;
};

export const fetchDealByUid = async (dealUid: string, clientUrl?: string): Promise<AxiosResponse<BackendDealResponse>> => {
    const type = getNetworkType();
    const finalUrl = clientUrl || API_URLS[type][0];
    const axiosParams: AxiosRequestConfig = {
        url: `${finalUrl}/api/deals/${dealUid}`,
        headers: getAuthHeaders(),
    };

    return axios(axiosParams);
};
interface StatusData {
    status: string;
    dealUid: string;
    attributes?: DealAttribute[];
}
export const fetchEditDeal = async (data: StatusData, clientUrl: string): Promise<AxiosResponse<Deal>> => {
    const axiosParams: AxiosRequestConfig = {
        url: `${clientUrl}/api/deals/${data.dealUid}`,
        headers: getAuthHeaders(),
        method: 'POST',
        data,
    };

    return axios(axiosParams);
};
export const fetchUploadFile = async (
    dealUid: string,
    fileData: { file: File },
    clientUrl: string,
): Promise<any> => {
    const formData = new FormData();
    formData.append('formFile', fileData.file);
    const axiosParams: AxiosRequestConfig = {
        url: `${clientUrl}/api/files`,
        headers: getAuthHeaders(),
        method: 'POST',
        data: formData,
    };
    const response = await axios(axiosParams);
    return response;
};
